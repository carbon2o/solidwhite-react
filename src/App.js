import './App.css';
import MagicWord from './components/magic-word/magic-word';
import StockData from './components/stock-data/stock-data';
import React, { useState } from 'react';

function App() {
  const mockStockData = [
    {
      company: "Twitter Inc",
      ticker: "TWTR",
      stockPrice: "22.76 USD",
      timeElapsed: "5 sec ago",
    },
    {
      company: "Square Inc",
      ticker: "SQ",
      stockPrice: "45.28 USD",
      timeElapsed: "10 sec ago",
    },
    {
      company: "Shopify Inc",
      ticker: "SHOP",
      stockPrice: "341.79 USD",
      timeElapsed: "3 sec ago",
    },
    {
      company: "Sunrun Inc",
      ticker: "RUN",
      stockPrice: "9.87 USD",
      timeElapsed: "4 sec ago",
    },
    {
      company: "Adobe Inc",
      ticker: "ADBE",
      stockPrice: "300.99 USD",
      timeElapsed: "10 sec ago",
    },
    {
      company: "HubSpot Inc",
      ticker: "HUBS",
      stockPrice: "115.22 USD",
      timeElapsed: "12 sec ago",
    },
  ];

  const [displayTask, setDisplayTask] = useState(1);
  const [randomArray, setRandomArray] = useState(mockStockData);
  
  // Generates random array by MockStockData's items and updates state
  function randomArrayGenerate () {
    let array = [];
    for (let i = 0; i < Math.floor(Math.random() * mockStockData.length); i++) {
      array.push(mockStockData[Math.floor(Math.random() * mockStockData.length)]);
    }
    setRandomArray(array);
  };

  return (
    <div className="App">
      <button className="btn-task-1" onClick={() => setDisplayTask(1)}>{"Task 1"}</button>
      <button className="btn-task-2" onClick={() => setDisplayTask(2)}>{"Task 2"}</button>

      { displayTask === 1 &&
        < StockData stockData={randomArray} randomArrayGenerate={() => randomArrayGenerate()} />
      }

      {displayTask === 2 &&
        <MagicWord />
      }
    </div>
  );
}

export default App;
