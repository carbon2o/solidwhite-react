import React from 'react';
import './stock-data.css';

class StockData extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            stockData: this.props.stockData,
        };
    }

    // Updates state when props changes and re-render
      componentDidUpdate(prevProps) {
        if(prevProps.stockData !== this.props.stockData) {
            this.setState({stockData: this.props.stockData});
          }
      } 

    // Triggers Parent component's randomArrayGenerator function
    dataChangeTest() {
        this.props.randomArrayGenerate();
    }

    render() {
        return (
            <div>
                <header className="stock-data-header">
                    {"SolidWhite Stock Data"}
                </header>
                <button className="btn-data-change" onClick={() => this.dataChangeTest()}>{"Change Data"}</button>

                <div className="stock-data-table">
                    <table>
                        <tbody>
                            <tr className="table-title-columns">
                                <th>{"Company"}</th>
                                <th>{"Ticker"}</th>
                                <th>{"Stock Price"}</th>
                                <th>{"Time Elapsed"}</th>
                            </tr>
                        </tbody>
                        <tbody>
                            {this.state.stockData.map((item, index) => (

                                <tr key={index} className="table-columns" style={(index % 2) === 1 ? { backgroundColor: '#dadada' } : { backgroundColor: 'white' }}>
                                    <td>{item.company}</td>
                                    <td>{item.ticker}</td>
                                    <td>{item.stockPrice}</td>
                                    <td>{item.timeElapsed}</td>
                                </tr>
                            ))}
                        </tbody>
                    </table>
                </div>
            </div>
        );
    }

}

export default StockData;