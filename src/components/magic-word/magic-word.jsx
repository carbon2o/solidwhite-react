import React from 'react';
import './magic-word.css';

class MagicWord extends React.Component {
    constructor() {
        super();
        this.state = {
            input: "",
            magicWord: "abracadabra",
            messageToShow: "You typed the magic word!",
            displayMode: true,
            view: null,

        };
    }

    // Update input value in the state
    handleChange(e) {
        this.setState({ input: e.target.value });
    }

    // Checks if the words are matching
    checkMagicWord() {
        if (this.state.input === this.state.magicWord) {
            if (this.state.displayMode) {
                this.setState({ view: <div className="magic-message"> {this.state.messageToShow} </div> });
                this.inputClear();
            } else {
                this.setState({ view: alert(this.state.messageToShow) });
                this.inputClear();
            }
        } else {
            this.setState({ view: <div className="wrong-message"> {"Charmless Word"} </div> });
            this.inputClear();
        }
    }

    // Trigger the check method with keyPressEnter
    handleKeyPress(event) {
        if (event.key === 'Enter') {
            this.checkMagicWord();
        }
    }

    // Display mode change between Alert - Text
    onDisplayModeChange(event) {
        this.setState({
            displayMode: !this.state.displayMode,
            view: "",
        });
        this.inputClear();
    }

    // Clears the input value in the state
    inputClear() {
        this.setState({ input: "" });
    }

    render() {
        return (
            <div>
                <header className="magic-word-header">
                    {"SolidWhite Stock Magic Word"}
                </header>

                <div className="radio-btn-group" onChange={this.onChangeDisplayMode}>
                    <input className="radio-btn-text" type="radio" checked={this.state.displayMode} onChange={this.onDisplayModeChange.bind(this)} /> <span>{"Text"}</span>
                    <input className="radio-btn-alert" type="radio" checked={!this.state.displayMode} onChange={this.onDisplayModeChange.bind(this)} /> <span>{"Alert"}</span>
                </div>

                <input className="input-magic-word" type="text" value={this.state.input} placeholder={'Enter the Magic word'} onChange={this.handleChange.bind(this)} onKeyPress={this.handleKeyPress.bind(this)} />
                <button className="btn-word-check" onClick={() => this.checkMagicWord()}>{"Check"}</button>

                {this.state.view}
            </div>
        );
    }

}

export default MagicWord;